import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import Navbar from './partials/Navbar';
import MembersPage from './pages/MembersPage';

const root = document.querySelector("#root")
	//<Fragment>
		//<h1 className="bg-pink">Hello, Batch 44!</h1>
		//<button className="btn btn-danger">bootstrap button</button>
		//<Button color="primary" className="ml-3">Reactstrap Button</Button>
	//</Fragment>

const pageComponent = (
	<Fragment>
		<Navbar/>
		<MembersPage/>
	</Fragment>
)

ReactDOM.render(pageComponent, root)
