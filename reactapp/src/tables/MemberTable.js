import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from './MemberRow'
import { Container, Row, Col } from 'reactstrap';


const Example = (props) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
          <MemberRow/>
        </tbody>
    </Table>
  );
}

export default Example;